# Aplicação para teste para: SharedPreference e TextInputEditText

Esse programa é uma aplicação para testar os comandos das duas classes.

## Referências 
- [SharedPreferences](https://developer.android.com/reference/android/content/SharedPreferences?hl=en)
- [Curso Udemy - SharedPreference](https://www.udemy.com/curso-de-desenvolvimento-android-oreo/)
- [FAB](https://developer.android.com/guide/topics/ui/floating-action-button)
- [TextInputEditText](https://developer.android.com/reference/android/support/design/widget/TextInputLayout.html)
- [TextInputEditText - AndroidHive](https://www.androidhive.info/2015/09/android-material-design-floating-labels-for-edittext)

### Adicionar Biblioteca de Design

```
    implementation 'com.android.support:design:28.0.0'

```

### Adicionar Para Usar o "vectorDrawables"

```
     buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
            android.defaultConfig.vectorDrawables.useSupportLibrary = true //Adicionar essa linha
        }
    }

```
