package com.example.testepreferencia;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.lang.String;

public class SegundaActivity extends AppCompatActivity {

    private TextInputEditText email, senha, telefone, mensagem, user, server, password;
    private FloatingActionButton fab;
    private String e,s,t,m, u, se, p;
    public static final String ARQUIVO_CONFIGURACAO = "ArquivoConfiguracao";
    public static final String EMAIL_CONFIGURACAO = "email";
    public static final String SENHA_CONFIGURACAO = "senha";
    public static final String TELEFONE_CONFIGURACAO = "telefone";
    public static final String MENSAGEM_CONFIGURACAO = "mensagem";
    public static final String SERVER_CONFIGURACAO = "server";
    public static final String USER_CONFIGURACAO = "user";
    public static final String PASSWORD_CONFIGURACAO = "password";
    private ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        SharedPreferences preferences = getSharedPreferences(ARQUIVO_CONFIGURACAO, 0 );
        final SharedPreferences.Editor editor = preferences.edit();

        constraintLayout = findViewById(R.id.layout);

        email = findViewById(R.id.edtEmail);
        senha = findViewById(R.id.edtSenha);
        telefone = findViewById(R.id.edtTelefone);
        mensagem = findViewById(R.id.edtMensagem);
        server = findViewById(R.id.edtServer);
        user = findViewById(R.id.edtUser);
        password = findViewById(R.id.edtPassword);

        fab = findViewById(R.id.btnFbtn);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                e = email.getText().toString();
                s = senha.getText().toString();
                t = telefone.getText().toString();
                m = mensagem.getText().toString();
                se = server.getText().toString();
                u = user.getText().toString();
                p = password.getText().toString();

                if (!e.isEmpty() && isValidEmail(e) && !s.isEmpty() && !t.isEmpty() && !m.isEmpty()
                && !se.isEmpty() && !u.isEmpty() && !p.isEmpty()){
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                    editor.putString(EMAIL_CONFIGURACAO, e);
                    editor.putString(SENHA_CONFIGURACAO, s);
                    editor.putString(TELEFONE_CONFIGURACAO, t);
                    editor.putString(MENSAGEM_CONFIGURACAO,m);
                    editor.putString(SERVER_CONFIGURACAO,se);
                    editor.putString(USER_CONFIGURACAO,u);
                    editor.putString(PASSWORD_CONFIGURACAO,p);
                    editor.commit();

                    startActivity(intent);
                    finish();

                }else{
                    Snackbar.make(constraintLayout, "Algum problema em um campo", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

        if (preferences.contains(EMAIL_CONFIGURACAO)){
            email.setText(preferences.getString(EMAIL_CONFIGURACAO, ""));
        }else {
            email.setText("");
        }

        if (preferences.contains(SENHA_CONFIGURACAO)){
            senha.setText(preferences.getString(SENHA_CONFIGURACAO, ""));
        }else {
            senha.setText("");
        }

        if (preferences.contains(TELEFONE_CONFIGURACAO)){
            telefone.setText(preferences.getString(TELEFONE_CONFIGURACAO, ""));
        }else {
            telefone.setText("");
        }

        if (preferences.contains(MENSAGEM_CONFIGURACAO)){
            mensagem.setText(preferences.getString(MENSAGEM_CONFIGURACAO, ""));
        }else {
            mensagem.setText("");
        }

        if (preferences.contains(SERVER_CONFIGURACAO)){
            server.setText(preferences.getString(SERVER_CONFIGURACAO, ""));
        }else {
            server.setText("");
        }

        if (preferences.contains(USER_CONFIGURACAO)){
            user.setText(preferences.getString(USER_CONFIGURACAO, ""));
        }else {
            user.setText("");
        }

        if (preferences.contains(PASSWORD_CONFIGURACAO)){
            password.setText(preferences.getString(PASSWORD_CONFIGURACAO, ""));
        }else {
            password.setText("");
        }


    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
