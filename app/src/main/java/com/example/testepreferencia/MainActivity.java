package com.example.testepreferencia;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static com.example.testepreferencia.SegundaActivity.ARQUIVO_CONFIGURACAO;
import static com.example.testepreferencia.SegundaActivity.EMAIL_CONFIGURACAO;
import static com.example.testepreferencia.SegundaActivity.MENSAGEM_CONFIGURACAO;
import static com.example.testepreferencia.SegundaActivity.PASSWORD_CONFIGURACAO;
import static com.example.testepreferencia.SegundaActivity.SENHA_CONFIGURACAO;
import static com.example.testepreferencia.SegundaActivity.SERVER_CONFIGURACAO;
import static com.example.testepreferencia.SegundaActivity.TELEFONE_CONFIGURACAO;
import static com.example.testepreferencia.SegundaActivity.USER_CONFIGURACAO;

public class MainActivity extends AppCompatActivity {

    private TextView getTxtEmail, getTxtSenha, getTxtTefone, getTxtMensagem,
    getTxtServer, getTxtUser, getTxtPassword;
    private FloatingActionButton floatingActionButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences(ARQUIVO_CONFIGURACAO, 0);

        floatingActionButton = findViewById(R.id.btnFloating);

        getTxtEmail = findViewById(R.id.txtEmail);
        getTxtSenha = findViewById(R.id.txtSenha);
        getTxtTefone = findViewById(R.id.txtTelefone);
        getTxtMensagem = findViewById(R.id.txtMensagem);
        getTxtServer = findViewById(R.id.txtServer);
        getTxtUser = findViewById(R.id.txtUser);
        getTxtPassword = findViewById(R.id.txtPassword);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SegundaActivity.class);
                startActivity(intent);
                finish();
            }
        });


        if (checkPreferences(preferences)) {
            getTxtEmail.setText(preferences.getString(EMAIL_CONFIGURACAO, ""));
            getTxtSenha.setText(preferences.getString(SENHA_CONFIGURACAO, ""));
            getTxtTefone.setText(preferences.getString(TELEFONE_CONFIGURACAO, ""));
            getTxtMensagem.setText(preferences.getString(MENSAGEM_CONFIGURACAO, ""));
            getTxtServer.setText(preferences.getString(SERVER_CONFIGURACAO, ""));
            getTxtUser.setText(preferences.getString(USER_CONFIGURACAO, ""));
            getTxtPassword.setText(preferences.getString(PASSWORD_CONFIGURACAO, ""));
        }

    }


    private Boolean checkPreferences(SharedPreferences preferences){
        if (preferences.contains(EMAIL_CONFIGURACAO) && preferences.contains(SENHA_CONFIGURACAO) &&
        preferences.contains(TELEFONE_CONFIGURACAO) && preferences.contains(MENSAGEM_CONFIGURACAO) &&
        preferences.contains(SERVER_CONFIGURACAO) && preferences.contains(USER_CONFIGURACAO) && preferences.contains(PASSWORD_CONFIGURACAO)){
            return true;
        }
            return false;
    }

}
